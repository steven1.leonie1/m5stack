from distutils import errors
import json
import serial
import requests as request
import PyQt5.QtWidgets as qtw

class Apps(qtw.QWidget):
 
    def __init__(self):
        super(Apps, self).__init__()
        self.setWindowTitle("Weather")
        self.setLayout(qtw.QVBoxLayout())
        self.initUI()
        self.show()

    def initUI(self):
        """
            This function is used to initiliaze ui of our program
        """
        container = qtw.QWidget()
        container.setLayout(qtw.QHBoxLayout())

        label_lat = qtw.QLineEdit()
        label_lat.setPlaceholderText('lattitude')
        label_lat.setStyleSheet("QLineEdit { "
                                "border: 1px solid #001427; "
                                "color : #001427;"
                                "font-size: 15px;"
                                "}")

        label_long = qtw.QLineEdit()
        label_long.setPlaceholderText('longitude')
        label_long.setStyleSheet("QLineEdit { "
                                 "border: 1px solid #001427; "
                                 "color : #001427;"
                                 "font-size: 15px;"
                                 "}")

        button = qtw.QPushButton("Send")
        button.setStyleSheet(
            "color: #001427; border: 1px solid #001427; width: 50px; font-size: 15px;")
        button.clicked.connect(lambda: self.getTempFromAPI(
            label_lat.text(), label_long.text()))

        container.layout().addWidget(label_lat)
        container.layout().addWidget(label_long)
        container.layout().addWidget(button)

        self.layout().addWidget(container)

    def sendTempToSerialPort(self, temp):
        """
            This function sends the recovered temperature to the serial port
            Args:
                - temp: temperature receive from api
        """
        temperature = str(temp)
        encoded = temperature.encode(encoding='ascii', errors='ignore')
        with serial.Serial(rtscts = False, dsrdtr = False) as ser:
            ser.baudrate = 115200
            ser.port = 'COM6'
            ser.dtr = 0
            ser.rts = 0
            ser.open()
            ser.write(encoded)
            ser.close()

    def getTempFromAPI(self, lat, long):
        """
            This function fetch data API  in JSON format
            Args:
                - lat : latitude get from input
                - long : longitude get from input
        """
        req = request.get(f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={long}&appid=0af0fdb86732575d2180ec3a8e1949d6');
        temp = self.kelvinToCelsius(req.json()['main']['temp'])
        self.sendTempToSerialPort(temp)

    def kelvinToCelsius(self, temp):
        """
            This method convert Kelvin to Celsius
            Args: 
                - temp : Kelvin temperature
        """
        return round(temp - 273.15, 1)

app = qtw.QApplication([])
application = Apps()
app.exec_()
