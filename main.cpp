#include <Arduino.h>
#include <M5Core2.h>
#include <string.h>
 
void setup() {
  M5.begin(true, true, true, true);
  M5.Lcd.fillScreen(WHITE);
  M5.Lcd.setTextColor(BLACK);
  M5.Lcd.setTextSize(4);
  M5.Lcd.drawString("Temp :", 50, 100);
  delay(100);
}
 
void loop(){
    if (Serial.available() > 0) {
      String tempString = Serial.readString();
      M5.Lcd.drawString("Temp : " + tempString , 50, 100);
    }
}